#!/bin/bash
# AUTHOR: Thomas Zink
# VERSION: 201512171
#
# This script provides timemachine like backup and restore functions
# Default configuration is written to ~/.rsync

#DATE=`date "+%Y-%m-%d_%H-%M-%S"`
DATE=`date "+%Y-%m-%d"`
ETC=~/.rsync

usage() {
	echo "usage: $0 [Options]"
	echo "Options:"
	echo "  -b          Backup"
	echo "  -r          Restore"
	echo "  -f <PATH>   Use configuration file"
	exit 0
}

create_links() {
	if [ -n "$HOST" ]; then
		ssh ${USER}@${HOST} \
		"mv ${DST}/${DATE}_running ${DST}/${DATE}_backup; \
		[[ -e ${DST}/current ]] && rm ${DST}/current; \
		cd ${DST}; \
		ln -sf ${DATE}_backup current;"
	else
		mv ${DST}/${DATE}_running ${DST}/${DATE}_backup
		[[ -e ${DST}/current ]] && rm ${DST}/current
		cd ${DST}
		ln -sf ${DATE}_backup current
	fi
}

backup() {
	if [ -n "$HOST" ]; then
		TARGET=${USER}@${HOST}:${DST}/${DATE}_running
	fi

	if [ -z "$HOST" ]; then
		TARGET=${DST}/${DATE}_running
	fi

	echo "backup to ${TARGET}";
	rsync -azP ${OPT} --include-from=${ETC}/include --exclude-from=${ETC}/exclude --log-file=${DST}/${DATE}_backup.log --link-dest=${DST}/current "${SRC}" ${TARGET}
	create_links
}

restore() {
	if [ -n "$HOST" ]; then
		RESTORE=${USER}@${HOST}:${DST}/current
	fi

	if [ -z "$HOST" ]; then
		RESTORE=${DST}/current
	fi

	echo "restore from ${RESTORE}"
	rsync -azP --log-file=${ETC}/log/${DATE}_restore.log ${RESTORE}/ ${SRC}
}

check_config() {
	[[ ! -e ${ETC} ]] && mkdir -p ${ETC}
	if [[ ! -e ${ETC}/default ]]; then
		printf "SRC=/home/`whoami`/\n" >> ${ETC}/default
		printf "DST=/mnt/`hostname`/`whoami`\n" >> ${ETC}/default
		printf "USER=`whoami`\n" >> ${ETC}/default
		printf 'HOST=""\n' >> ${ETC}/default
		printf 'OPT="--delete --delete-excluded"\n' >> ${ETC}/default
	fi
	[[ ! -e ${ETC}/include ]] && touch ${ETC}/include
	[[ ! -e ${ETC}/exclude ]] && touch ${ETC}/exclude
	. ${ETC}/default
}

check_config

while getopts "hbrf:" OP; do
	case $OP in
		h)
			usage
			;;
		b)
			FUNC=backup
			;;
		r)
			FUNC=restore
			;;
		f)
			. ${ETC}/$OPTARG
			;;
		?)
			usage
			;;
	esac
done

$FUNC
