#!/usr/bin/env python3
###
# calc_transfertime
# calculates the transfer time of a specific amount of data through a line.
#
# author: tz@uni.kn
#
###

import argparse

parser = argparse.ArgumentParser(description="Calculate transfertime of data though a line")
parser.add_argument('-d', help="amount of data [Bytes]", required=False)
parser.add_argument('-t', help="throughput of line [bit/s]", required=False)

def bit2Byte(bit):
    return bit/8.

def Byte2bit(Byte):
    return Byte*8.

def sec2h(sec):
    return sec/60./60.

def toGiga(data):
    return data/1000./1000./1000.

def transferTime(data,throughput):
    return round(sec2h(data/float(throughput)),2)

def print_result(data, throughput, time):
    print("Data: ", data, " Throughput: ", throughput, " Time: ", time)
    

def test():
    Speed = { "max": bit2Byte(4.2*10**9), "avg": bit2Byte(3.2*10**9) }
    Data = { "capacity": 520*10.**12, "full": 200*10.**12, "min": 40*10.**9, "max": 500*10.**9 }
    Data["mean"] = (Data["min"]+Data["max"])/2.

    print("Line Speeds [Gb/s]: ")
    for k in Speed.keys():
        print("- ", k, ": ", toGiga(Byte2bit(Speed[k])))

    print("")
    print("Data to transfer [GB]: ")
    for k in Data.keys():
        print("- ", k, ": ", toGiga(Data[k]))

    print("")
    print("Transfer Time [h]:")
    for d in Data.keys():
        for s in Speed.keys():
            print_result(str(Data[d]), Speed[s], transferTime(Data[d],Speed[s]))

def main(args):
    if args['t']==None and args['d']==None:
        parser.print_help()
        print("\nRunning a test:\n")
        test()
        exit(0)
    else:
        data=float(args['d'])
        tput=float(args['t'])
        time=transferTime(data,tput)
        print_result(data,tput,time)
    

if __name__ == '__main__':
    args = vars(parser.parse_args())
    main(args)
