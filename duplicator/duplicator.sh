#!/bin/bash
###
# duplicity incremental and full backups including cleanup
# author: tz
# versions:
# 	2016-02-18 - tz - initial version
#
# notes:
# https://www.digitalocean.com/community/tutorials/how-to-use-duplicity-with-gpg-to-securely-automate-backups-on-ubuntu
# https://help.ubuntu.com/community/DuplicityBackupHowto
###

DATE=`date "+%Y-%m-%d_%H-%M-%S"`
ETC=~/.duplicity

test -x $(which duplicity) || exit 0
DUP=$(which duplicity)

usage() {
	echo "usage: $0 [Options]"
	echo "Options:"
	echo "  -b          Backup"
	echo "  -r          Restore"
	echo "  -f <PATH>   Use configuration file"
	exit 0
}

load_config() {
	if [[ -e $1 ]]; then
		. $1
		if [ -z "$PASSPHRASE" ]; then export PASSPHRASE; fi
		if [ -n "$ENCRYPT_KEY" ]; then OPT="--encrypt-key $ENCRYPT_KEY"; fi
	fi
}

check_config() {
	if [[ ! -e ${ETC} ]]; then
		mkdir -p ${ETC} && chmod 700 ${ETC}
		touch ${ETC}/default && chmod 600 ${ETC}/default
		printf 'PASSPHRASE=""\n' >> ${ETC}/default
		printf 'ENCRYPT_KEY=""\n' >> ${ETC}/default
		printf "USER=`whoami`\n" >> ${ETC}/default
		printf 'HOST=""\n' >> ${ETC}/default
		printf "SRC=/home/`whoami`/\n" >> ${ETC}/default
		printf "DST=/mnt/backup/`hostname`/`whoami`\n" >> ${ETC}/default
		printf "FILELIST=${ETC}/filelist" >> ${ETC}/default
		touch ${ETC}/filelist
		echo "/home/`whoami`/.dbus" >> ${ETC}/filelist
		echo "/home/`whoami`/.gvfs" >> ${ETC}/filelist
		echo "/home/`whoami`/.cache" >> ${ETC}/filelist
	fi

	load_config ${ETC}/default
}

backup() {
	if [ -n "$HOST" ]; then
		TARGET=sftp://${USER}@${HOST}/${DST}
	fi

	if [ -z "$HOST" ]; then
		TARGET=file://${DST}
	fi

	echo "backup to ${TARGET}"
	printf "$DUP $OPT --exclude-filelist ${FILELIST} --verbosity info ${SRC} ${TARGET}"
	$DUP $OPT --exclude-filelist ${FILELIST} --verbosity info ${SRC} ${TARGET}
}

restore() {
	echo "stub"
}

check_config

while getopts "hbrf:" OP; do
	case $OP in
		h)
			usage
			;;
		b)
			FUNC=backup
			;;
		r)
			FUNC=restore
			;;
		f)
			load_config $OPTARG
			;;
		?)
			usage
			;;
	esac
done

$FUNC
