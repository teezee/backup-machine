#!/bin/bash
# VERSION: 2016031101
# AUTHOR: tz

CLOUD="~/nextcloud"
DIRS=( Desktop Documents Music Pictures Videos )

folders() {
	for i in ${DIRS[@]}; do
		mkdir -p ${CLOUD}/${i}
		rsync -var ~/${i}/* ${CLOUD}/${i}/
		rm -rf ~/${i}
		ln -s ${CLOUD}/${i} ~/${i}
	done
}

bash() {
	for i in `ls -A ${CLOUD}/Bash`; do
		if [ -f ${CLOUD}/Bash/${i} ]; then
			rm ~/${i}
		fi
		if [ -d ${CLOUD}/Bash/${i} ]; then
			rsync -var ~/${i} ${CLOUD}/Bash/
			rm -r ~/${i}
		fi
		ln -s ${CLOUD}/Bash/${i} ~/${i}
	done
}

folders;
bash;
