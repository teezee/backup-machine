#!/bin/bash
###
# Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

set -euf -o pipefail
ME=`basename "$0"`
LOGFILE="/var/log/$ME.log"

# REMOTE OPTIONS
SSHUSER="user"
SSHKEY="id_user"
HOST="0.0.0.0"

RSYNCPATH="/usr/bin/sudo /usr/bin/rsync"

# COMMON OPTIONS
OPTIONS=""
INCL="includes"
EXCL="excludes"
SRC="/"
DEST="/media/backups"

rsync -vaP -z ${OPTIONS} -e "ssh -i $SSHKEY" --rsync-path="$RSYNCPATH" --delete --log-file=$LOGFILE --exclude-from=$EXCL --include-from=$INCL --numeric-ids $SRC $SSHUSER@$HOST:$DEST/
