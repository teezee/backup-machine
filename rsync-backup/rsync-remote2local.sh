#!/bin/bash

set -eu -o pipefail
shopt -s failglob

REMOTESERVER="remote.server"
SSHUSER="user"
SSHKEY="id_user"
RSYNCPATH="/usr/bin/sudo /usr/bin/rsync"
EXCL="excludes"
INCL="includes"
SRC="/"
DST="/media/backup/"

# This command rsync's files from the remote server to the local server.
# Flags:
#   -z enables gzip compression of the transport stream.
#   -e enables using ssh as the transport prototcol.
#   --rsync-path lets us pass the remote rsync command through sudo.
#   --archive preserves all file attributes and permissions.
#   --exclude-from points to our configuration of files and directories to skip.
#   --numeric-ids is needed if user ids don't match between the source and
#       destination servers.
#   --link-dest is a key flag. It tells the local rsync process that if the
#       file on the server is identical to the file in ../$YESTERDAY, instead
#       of transferring it create a hard link. You can use the "stat" command
#       on a file to determine the number of hard links. Note that when
#       calculating disk space, du includes disk space used for the first
#       instance of a linked file it encounters. To properly determine the disk
#       space used of a given backup, include both the backup and it's previous
#       backup in your du command.
#
# The "rsync" user is a special user on the remote server that has permissions
# to run a specific rsync command. We limit it so that if the backup server is
# compromised it can't use rsync to overwrite remote files by setting a remote
# destination. I determined the sudo command to allow by running the backup
# with the rsync user granted permission to use any flags for rsync, and then
# copied the actual command run from ps auxww. With these options, under
# Ubuntu, the sudo line is:
#
#   rsync	ALL=(ALL) NOPASSWD: /usr/bin/rsync --server --sender -logDtprze.iLsf --numeric-ids . /
#
# Note the NOPASSWD option in the sudo configuration. For remote
# authentication use a password-less SSH key only allowed read permissions by
# the backup server's root user.
rsync -v -z -e "ssh -i $SSHKEY" \
	--rsync-path="$RSYNCPATH" \
	--archive \
	--delete \
	--exclude-from=$EXCL \
	--include-from=$INCL \
	--numeric-ids \
	--log-file=$REMOTESERVER/backup.log \
	${SSHUSER}@${REMOTESERVER}:${SRC} ${DST}

