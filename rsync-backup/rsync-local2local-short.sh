#!/bin/bash

set -euf -o pipefail

ME=`basename "$0"`

LOGFILE="/var/log/$ME.log"
OPTIONS=""
INCL="includes"
EXCL="excludes"
SRC="/"
DEST="/media/backups"

rsync -vapP ${OPTIONS} --delete --log-file=$LOGFILE --include-from=$INCL --exclude-from=$EXCL $SRC $DEST/
