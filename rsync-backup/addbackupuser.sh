#!/bin/bash
###
# Copyright (c) 2016-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# addbackupuser.sh
# Add user to system, create directories, add restriction to sudoers
###

set -eu -o pipefail
shopt -s failglob

backupgrp="backup-users"

# check if we got a backup group, add if not
getent group $backupgrp || groupadd $backupgrp


# check if we got a username
user="$1"
[[ -z "$user" ]] && echo "usage: `basename $0` USERNAME [GROUPS..]" && exit 0;

# check if we got any groups
groups=""
if [ "$#" -gt 1 ]; then
        if [ ! -z "$2" ]; then
                shift
                groups=( "$@" )
        fi
fi

# add a new user with name $user, create home (-m), use bash as shell (-s /bin/bash)
echo "useradd: Add user ${user}"
pass=$(pwgen 20 1)
sudo useradd -m -s /bin/bash -p $(openssl passwd -1 ${pass}) $user

# add to groups
usermod -aG $backupgrp $user
echo "usermod: Add user ${user} to group ${backupgrp}"

if [ ! -z "$groups" ]; then
        for grp in "${groups[@]}"; do
                echo "usermod: Add user ${user} to group ${grp}"
                sudo usermod -aG $grp $user
        done
fi

# create .ssh, authorized_keys
sudo mkdir -p -v /home/$user/.ssh
sudo touch /home/$user/.ssh/authorized_keys
echo "touch: create /home/${user}/.ssh/authorized_keys"

# set user:group and permissions
sudo chown -R $user:$user /home/$user/.ssh
echo "chown: set ${user}:${user} for /home/${user}/.ssh"
sudo chmod 700 /home/$user/.ssh
echo "chmod: set 700 for /home/${user}/.ssh"
sudo chmod 600 /home/$user/.ssh/authorized_keys
echo "chmod: set 600 for /home/${user}/.ssh/authorized_keys"

# add to sudoers