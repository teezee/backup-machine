#!/bin/bash
# script to open reverse ssh tunnel on target machine

# references
# https://www.g-loaded.eu/2006/11/24/auto-closing-ssh-tunnels/
# https://www.everythingcli.org/ssh-tunnelling-for-fun-and-profit-autossh/
# 

# variables
ID="id_reverse"


# on source: create ssh keys
ssh-keygen -t ed25519 -f $ID

# on destination: add restricted user
# useradd -m -s /bin/bash reverse

# on destination: add to /etc/ssh/sshd_config
# AllowUsers reverse
# match User reverse
#        AllowAgentForwarding no
#        AllowTcpForwarding yes
#        PermitOpen localhost:22222
#        ForceCommand echo 'restricted account'
#        ForceCommand sleep 120 # keep open only 120 secs 

# keep tunnel open
ssh -i $ID -tNTR 22222:localhost:22 reverse@target.server

# open auto closing tunnel (keeps open until time has passed or remote side closes connection)
ssh -i $ID -fR 22222:localhost:22 reverse@target.server sleep 120;

