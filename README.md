# BACKUP MACHINE

Multiple (semi)-autmatic backup strategies.


## cloudsync

Moves your personal files to a cloud synced directory (eg ownCloud) and creates symbolic links.
Set the cloud sync directory like this

```
CLOUD=~/nextcloud
```


## duplicator

Incremental and full backups including cleanup based on duplicity.
Requires a config file in `.duplicty/`. If not available it is written with the following default values.

```
PASSPHRASE=""
ENCRYPT_KEY=""
USER=`whoami`
HOST=""
SRC=/home/`whoami`
DST=/mnt/backup/`hostname`/`whoami`
FILELIST=${ETC}/filelist
```


## backup-servername

his is a simple backup script that uses rsync to backup files and does
complete mysql table dumps. Note that this script DOES NOT EXPIRE OLD BACKUPS.

Original source: https://gist.github.com/deviantintegral/0f1066650e3ea5c5ffc1


## rsync-backup

Multiple scripts to perform backups using rsync. Backups can be performed using the following strategies:

- local to local
- local to remote (push to remote backup server)
- remote to local (fetch backups from remote server)
- local to remote fake super with ssh tunnel


## syncmachine

A timemachine-like backup script that backups data to a local or remote directory.


## References

- [zen-and-the-art-of-it-backup](http://www.connections.com/blog/21-it-backup/100-zen-and-the-art-of-it-backup-principle-1)
- [The Tao of Backups](http://taobackup.com/)
- [roundup-of-remote-encrypted-deduplicated-backups](http://changelog.complete.org/archives/9353-roundup-of-remote-encrypted-deduplicated-backups-in-linux)
- [rsync-time-backup](https://github.com/laurent22/rsync-time-backup)
- [backup-servername.sh](https://gist.github.com/deviantintegral/0f1066650e3ea5c5ffc1)
- [Duplicity Backup Howto](https://help.ubuntu.com/community/DuplicityBackupHowto)
- [duplicity-backup.sh](https://zertrin.org/projects/duplicity-backup/)
- [backupninja](https://uname.pingveno.net/blog/index.php/post/2015/02/17/Set-up-files-and-database-incremental-backup-with-duplicity,-rsync,-and-backupninja-on-Debian)
- [obnam](http://obnam.org/)
- [attic-backup](https://attic-backup.org/)
- [borgbackup](https://borgbackup.readthedocs.io/en/stable/)
- [restic](https://restic.github.io)
- [unison](https://help.ubuntu.com/community/Unison)
- [rsync as root with rrsyn](https://www.v13.gr/blog/?p=216)
- [Preserve ownership with rsync](https://serverfault.com/questions/755753/preserve-ownership-with-rsync-without-root)
- [rsync and --fake-super](https://serverfault.com/questions/119846/rsync-and-fake-super-how-to-preserve-attributes)
- [restricting ssh so rsync](https://www.guyrutenberg.com/2014/01/14/restricting-ssh-access-to-rsync/)

